import os
import time
import json
import glob
import requests
import helpers
import numpy as np
import cv2

'''
    Use this code for inference with DTS in new images without previous labels. It will print the vehicle type, the name of the image and the score. 
'''

if __name__ == "__main__":    

    #Change the name of the folder    
    Image_path =  "/home/portico/AI/models-master/research/object_detection/RID-OCR/imagesTest/*.jpg"
    TEST_IMAGE_PATHS = glob.glob(Image_path)

    #Let's use our inference graph for classify every image on the path.
    for image in TEST_IMAGE_PATHS:
        try:
            #Measure time.
            start = time.time()
            # 1. First, make the post petition and save JSON response on r and load it on raux.
            url = 'http://0.0.0.0:6666'
            file_to_send = [('file', open(image, 'rb') )]
            image_cv2 = cv2.imread(image)
            r = requests.post(url, files = file_to_send)
            raux = json.loads(r.text)

            path = image.split("/")[-1]

            # 2. Let's take a look on the JSON. First, we have to check if we have made any classification.
            if raux['result'][0]['prediction'] != None:
                # Then, save the results on a variable because we want to see all the result and pick up the best one.
                results = raux["result"][0]["prediction"]
                #print(results)

                for result in results:
                    xmin = result['xmin']
                    xmax = result['xmax']
                    ymin = result['ymin']
                    ymax = result['ymax']
                    score = result['score']

                    #Draw boxes on image
                    print_id = str(int(score))
                    box_color = (0, 255, 0)
                    show_label = False
                    box = [ymin, xmin, ymax, xmax]
                    image_cv2 = helpers.draw_box_label(image_cv2, np.array(box), box_color, show_label, print_id)
            
                end = time.time()
                path_to_save = "/home/portico/AI/models-master/research/object_detection/RID-OCR/resultados/" + path 
                cv2.imwrite(path_to_save, image_cv2)

        except:
            pass
        


# #FOR CLASF


# if __name__ == "__main__":    

#     #Change the name of the folder    
#     Image_path =  "/home/portico/AI/models-master/research/object_detection/RID-OCR/images/*.jpg"
#     TEST_IMAGE_PATHS = glob.glob(Image_path)

#     #Let's use our inference graph for classify every image on the path.
#     for image in TEST_IMAGE_PATHS:
#         #Measure time.
#         start = time.time()
#         # 1. First, make the post petition and save JSON response on r and load it on raux.
#         url = 'http://0.0.0.0:6666'
#         file_to_send = [('file', open(image, 'rb') )]
#         r = requests.post(url, files = file_to_send)
#         raux = json.loads(r.text)

#         path = image.split("/")[-1]

#         # 2. Let's take a look on the JSON. First, we have to check if we have made any classification.
#         if raux['result'][0]['prediction'] != None:
#             # Then, save the results on a variable because we want to see all the result and pick up the best one.
#             results = raux["result"][0]["prediction"]
#             score = 0
#             vehicle_type = None
#             for result in range(len(results)):
#                 score_res = results[result]["score"]
#                 # Finally, choose the best score and the type associated.
#                 if score_res > score:
#                     score = score_res
#                     vehicle_type = results[result]["label"]
#                 #print(score, vehicle_type)

#         # 3. Compare the most probably label with the label that had. If it's different move the image to a another folder with the correct label.
        
#         path_to_save = " /home/vod/Documentos/clasf_cortina/traza_prueba_modelo_mixto/" + vehicle_type + "/"

#         command_var = "mv " + image + path_to_save
#         os.system(command_var)
#         end = time.time()
        
#         # 4. Print information for use it in a excel/csv and study it. -> score, label, path, change = yes or not
#         print(vehicle_type, ',', path,',', score)