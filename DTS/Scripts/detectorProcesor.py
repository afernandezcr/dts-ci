from flask import Flask, request
import json
import datetime
import numpy as np
import tensorflow as tf
import time
from PIL import Image

import logging
import helpers

def createLog(text):
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S.%f')
    print("[",st,"] -", text)

def load_graph(model_file):
    graph = tf.Graph()

    with tf.gfile.FastGFile(model_file, 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        with graph.as_default():
            _ = tf.import_graph_def(graph_def, name='')
    return graph

def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size

  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)

#No es necesario usar esta funcion porque podemos cargar la imagen directamente en rgb
def load_image_into_numpy_array_grayscale(img):
    (im_width, im_height) = img.size
    new_img = np.zeros((im_height,im_width,3))
    image1 = np.array(image.getdata()).reshape((im_height, im_width, 1)).astype(np.uint8)
    
    for xx in range(im_height):
        for yy in range(im_width):            
            new_img[xx,yy,0] = image1[xx,yy] 
            new_img[xx,yy,1] = image1[xx,yy] 
            new_img[xx,yy,2] = image1[xx,yy] 
                
    return new_img

def f_equalize(image_np):
    aux = 0
    max = (255) * .5
    min = (255) * 0

    for i in range(len(image_np)):
      for j in range(len(image_np[0])):
           pixel = image_np[i][j][0] 
           if(((255)*(pixel-min)/(max-min))>(255)):
               aux = (255)
           else:
               if(((255) *(pixel-min)/(max-min))<=0):
                    aux = 0
               else:
                    aux = (255) *(pixel-min)/(max-min)

           image_np[i][j][0] = aux
           image_np[i][j][1] = aux
           image_np[i][j][2] = aux  

def recvall(sock, count):
    buf = b''
    while count:
        newbuf = sock.recv(count)
        if not newbuf: return None
        buf += newbuf
        count -= len(newbuf)
    return buf

def recvSize(sock):
    buf = b''
    count = int(sock.recv(1))
    while count:
        newbuf = sock.recv(count)
        if not newbuf: return None
        buf += newbuf
        count -= len(newbuf)
    return buf

PATH_TO_TEST_IMAGES_DIR = 'test_images'
IMAGE_SIZE = (16, 12)

# Initialize the Flask application
app = Flask(__name__)

sess = 0
detection_boxes = 0
detection_scores = 0
detection_classes = 0
num_detections = 0
image_tensor = 0
DebugMode = False

@app.route("/init", methods=["POST"])
def run():

    global sess
    global detection_boxes 
    global detection_scores 
    global detection_classes 
    global num_detections 
    global image_tensor
    global DebugMode 

    jsondata = request.get_json()
    data = json.loads(jsondata)
    PATH_TO_CKPT = data[0]['model_path']
    DebugMode = data[0]['DebugMode']

    data2 = json.loads(data[0]["ObjectTracking"])
    if data2['enabled'] == "true":
        print(data2['features'][0]['direction'])
        print(data2['features'][0]['min_hits'])
        print(data2['features'][0]['maxAge'])
    
    #config = tf.ConfigProto()
    #config.gpu_options.allow_growth = True
    detection_graph = load_graph(PATH_TO_CKPT)
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=float(data[0]['MemoryUsage']))
    config=tf.ConfigProto(gpu_options=gpu_options)
    sess = tf.Session(graph=detection_graph, config=config)

    # Definite input and output Tensors for detection_graph
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
    # Each box represents a part of the image where a particular object was detected.
    detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
    # Each score represent how level of confidence for each of the objects.
    # Score is shown on the result image, together with the class label.
    detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
    detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')
    
    result = {'response': True}
    return json.dumps(result)

@app.route("/", methods=["POST"])
def home():

    global sess
    global detection_boxes 
    global detection_scores 
    global detection_classes 
    global num_detections 
    global image_tensor

    img = Image.open(request.files['file']).convert('RGB')
    #img.save("test", "JPEG")
    image_np = np.array(img)

    #createLog("Image received " + str(len(image_np[0])) + "x" + str(len(image_np))) 
    logger = helpers.get_logger('detectorProcesor')
    logger.info("Image received " + str(len(image_np[0])) + "x" + str(len(image_np))) 

    info = []
    #equalize = True
    start_time = time.time()
  
    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
    image_np_expanded = np.expand_dims(image_np, axis=0)
    # Actual detection.
    (boxes, scores, classes, num) = sess.run(
        [detection_boxes, detection_scores, detection_classes, num_detections],
        feed_dict={image_tensor: image_np_expanded})
    end=time.time()
    cont = 0
    numParabrisas = 0
    for i in range(len(scores[0])):
        if((scores[0][i]) > 0.15):
            cont +=1
    for i in range(cont):
        ymin = boxes[0][i][0]*len(image_np)
        xmin = boxes[0][i][1]*len(image_np[0])
        ymax = boxes[0][i][2]*len(image_np)
        xmax = boxes[0][i][3]*len(image_np[0])
        score = (scores[0][i] * 100)
        if(int(xmax-xmin) < 900):# and equalize):
          numParabrisas+=1
          #info.append(str(int(xmin)) + "," + str(int(ymin)) + "," + str(int(xmax-xmin)) + "," + str(int(ymax-ymin)) +"," + str(int(score)) + ";")
          dataAux = {"label" : "windshield", "xmin" : int(xmin), "ymin" : int(ymin), "xmax" : int(xmax), "ymax" : int(ymax), "score" : float(score)}
          info.append(dataAux)
          #equalize = False
          #print("---%s seconds -----" % (time.time() - start_time))              
    #equalize = False      
            
    #str1 = str(numParabrisas) + "-" +' '.join(info) + '\n'
    #print("Parabrisas send:", str1)

    milisg=(end - start_time)
    milisg=milisg*1000

    #createLog("Inference Time " + str(milisg) + " ms")
    logger = helpers.get_logger('detectorProcesor')
    logger.info("Inference Time " + str(milisg) + " ms") 


    #s = str(request.files['image'])
    #if(len(s) > 0):
    #    s = (s.split("FileStorage: '", 1))[1]
    #    s = (s.split("' ('image/jpeg')>", 1))[0]
    

    path_image = "test.jpg"
    data = {"message":"Success","result":[{"message":"success","input": path_image,"prediction": info}]}

    json_data = json.dumps(data, sort_keys=True) + '\n'
    #createLog ("Results sended: " + str(data))
    logger = helpers.get_logger('detectorProcesor')
    logger.info("Results sended: " + str(data)) 

    return json_data

