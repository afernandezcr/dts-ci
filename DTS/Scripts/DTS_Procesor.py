#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from waitress import serve
import xmlschema
import xml.etree.ElementTree as ET
#from threading import Thread
from multiprocessing import Process
import time 
import json
import requests
import datetime
import clasfProcesor
import trackerMain
import detectorProcesor
from flask import Flask, request

import logging
import helpers

DebugMode = False

app = Flask(__name__)

activateModels = []
activateThreads = []

def createLog(text):

    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S.%f')
    print("[",st,"] -", text)

def serveFunction(ip,port,modelType):
    if modelType == "clasf":
        serve(clasfProcesor.app, host=ip, port=port)
    elif modelType == "detec":
        serve(detectorProcesor.app, host=ip, port=port)
    elif modelType == "tracking":
        serve(trackerMain.app, host=ip, port=port)

def initializeModelsFunction(url):

    #createLog("Initializing model " + url + " using an image")
    logger = helpers.get_logger('DTS_Procesor')
    logger.info("Initializing model " + url + " using an image")
    
    image="img_lateral_right_48590_Fri_Dec_29_09_36_58_2017_te_0.49_g_6.97_s_0.00_b_0.00.jpg"
    file_to_send = [('file', open(image, 'rb') )]
    r = requests.post(url, files = file_to_send)
    raux = json.loads(r.text)

    #createLog("the initialize using an image was " + raux["message"])
    logger = helpers.get_logger('DTS_Procesor')
    logger.info("the initialize using an image was " + raux["message"])

def check_connection():

    DTS_HOST= "0.0.0.0"
    DTS_PORT= 8050

    try:
        while True:
            time.sleep(10)
            urlInit = 'http://' + DTS_HOST +':' + str(DTS_PORT) + '/getActive'
            r = requests.get(urlInit)
            raux = json.loads(r.text)
            for url in raux["activatedModels"]:
                try:
                    requests.post(url, timeout=1)
                    #createLog("The connection " + url +" is ok")
                    logger = helpers.get_logger('DTS_Procesor')
                    logger.info("The connection " + url +" is ok")
                except:
                    #createLog("The connection " + url + " is down")
                    logger = helpers.get_logger('DTS_Procesor')
                    logger.warning("The connection " + url +" is down")
                     
    except:
        #createLog("There was an error during the comprobation")
        logger = helpers.get_logger('DTS_Procesor')
        logger.error("There was an error during the comprobation")
         

def createModel(name, IP, Port, info, modelType):
    #createLog("Model" + name + "detected, for runing on " + IP + ":" + Port)
    logger = helpers.get_logger('DTS_Procesor')
    logger.info("Model " + name + " detected, for runing on " + IP + ":" + Port)

    thread = Process(target = serveFunction, args = (IP, int(Port),modelType))
    thread.start()
    activateThreads.append(thread)
    time.sleep(1)
    s = json.dumps(info)
    urlInit = 'http://' + IP +':' + Port + '/init'
    url = 'http://' + IP +':' + Port
    activateModels.append(url)
    req = requests.post(urlInit, json=s).json()
    initializeModelsFunction(url)
    return thread
    #print(req['response'])

def createDetecfModel(name, IP, Port, info):
    #createLog("Model " + name + " detected, for runing on " + IP + " : " + Port + " using tracking flag")
    logger = helpers.get_logger('DTS_Procesor')
    logger.info("Model " + name + " detected, for runing on " + IP + " : " + Port + " using tracking flag")

    thread1 = Process(target = serveFunction, args = (IP, int(Port),"tracking"))
    thread1.start()
    activateThreads.append(thread1)
    time.sleep(1)
    s = json.dumps(info)
    print(s)
    urlInit = 'http://' + IP +':' + Port + '/init'
    url = 'http://' + IP +':' + Port
    req = requests.post(urlInit, json=s).json()
    activateModels.append(url)
    initializeModelsFunction(url)

@app.route("/init", methods=["POST"])
def run():

    global DebugMode

    jsondata = request.get_json()
    data = json.loads(jsondata)
    tree = ET.parse(data[0]['filename_xml'])
    root = tree.getroot()
    DebugMode = root[1].text

    #check clasf models
    for child in root[0]:

        info = [{"DebugMode" : DebugMode,child[0].tag: child[0].text, child[1].tag: child[1].text, child[2].tag: child[2].text, child[3].tag: child[3].text, "Name":child.attrib["name"]}]
        createModel(child.attrib["name"], child.attrib["IP"], child.attrib["Port"], info,"clasf")
        
    for child in root[2]:

        if child[0].attrib["enabled"] == "true":
            data = {"enabled":child[0].attrib["enabled"],"features":[{child[0][0].tag:child[0][0].text,child[0][1].tag:child[0][1].text,child[0][2].tag:child[0][2].text}]}
            data = json.dumps(data)
            info = [{"ObjectTracking": data,"DebugMode" : DebugMode,child[1].tag: child[1].text, child[2].tag: child[2].text, child[3].tag: child[3].text, child[4].tag: child[4].text, child[5].tag: child[5].text}]
            createModel(child.attrib["name"], child.attrib["IP"], child.attrib["Port"], info,"tracking")

        else:
            data = {"enabled":child[0].attrib["enabled"],"features":[]}
            data = json.dumps(data)
            info = [{"ObjectTracking": data,"DebugMode" : DebugMode,child[1].tag: child[1].text, child[2].tag: child[2].text, child[3].tag: child[3].text, child[4].tag: child[4].text, child[5].tag: child[5].text}]
            createModel(child.attrib["name"], child.attrib["IP"], child.attrib["Port"], info,"detec")

    thread3 = Process(target = check_connection)
    thread3.start()

    if len(activateThreads) > 0:
        activateThreads[0].join()

    result = {'response': True}
    return json.dumps(result)

@app.route("/shutdownService", methods=["POST"])
def shutdownService():
    result = {'response': False}

    #createLog("Recieved shutdownService, showing activated Microservices")
    logger = helpers.get_logger('DTS_Procesor')
    logger.info("Recieved shutdownService, showing activated Microservices")

    jsondata = request.get_json()
    for key, value in jsondata.items():
        if key == "Service":
            try:
                index = activateModels.index(value)
                activateThreads[index].terminate()
                del activateThreads[index]
                del activateModels[index]
                result = {'response': True}
                #createLog(value + " has been stoped fine")
                logger = helpers.get_logger('DTS_Procesor')
                logger.info(value + " has been stoped fine")
            except:
                #createLog("Error," +  value + " is not in list")
                logger = helpers.get_logger('DTS_Procesor')
                logger.warning(value + " is not in list")
                 
        else:
            #createLog("Error, the Key is not valid")
            logger = helpers.get_logger('DTS_Procesor')
            logger.error("Error, the Key is not valid")
             
    return json.dumps(result)

@app.route("/newService", methods=["POST"])
def newService():
    global DebugMode
    result = {'response': False}
    #createLog("Recieved newService, trying to start")
    logger = helpers.get_logger('DTS_Procesor')
    logger.info("Recieved newService, trying to start")

    jsondata = request.get_json()
    dic = {}
    for key, value in jsondata.items():
        try:
            dic[key] = value
        except:
            #createLog("Error, " + value + " is not in list")
            logger = helpers.get_logger('DTS_Procesor')
            logger.warning("Error, " + value + " is not in list")
             

    exist = False
    for model in activateModels:
        if "http://" + dic["IP"] + ":" + dic["Port"] == model:
            exist = True

    if not exist:
        if dic["ServiceType"] == "clasf":
            try:
                info = [{"DebugMode" : DebugMode, "Path_labels": dic["Path_labels"], "MemoryUsage": dic["MemoryUsage"], "model_path": dic["model_path"], "MinScore": dic["MinScore"]}]
                createModel(dic["name"], dic["IP"], dic["Port"], info,"clasf")
            except:
                #createLog("Error on imput json")
                logger = helpers.get_logger('DTS_Procesor')
                logger.warning("Error on imput json")
                 

        elif dic["ServiceType"] == "detec":
            dic_ObjectTracking = {}
            for key, value in dic["ObjectTracking"].items():
                try:
                    dic_ObjectTracking[key] = value
                except:
                    #createLog("Error, " + value + " is not in list")
                    logger = helpers.get_logger('DTS_Procesor')
                     
                    logger.warning("Error," + value + " is not in list")
                    
            try:       
                if dic_ObjectTracking["enabled"] == "true":
                    data = {"enabled":dic_ObjectTracking["enabled"],"features":dic_ObjectTracking["features"]}#"features":[{child[0][0].tag:child[0][0].text,child[0][1].tag:child[0][1].text,child[0][2].tag:child[0][2].text}]}
                    data = json.dumps(data)
                    info = [{"ObjectTracking": data,"DebugMode" : DebugMode,"MinScore": dic["MinScore"], "MinDetectionSize": dic["MinDetectionSize"], "MemoryUsage": dic["MemoryUsage"], "model_path": dic["model_path"]}]
                    createModel(dic["name"], dic["IP"], dic["Port"], info,"tracking")
                else:
                    data = {"enabled":dic_ObjectTracking["enabled"],"features":[]}
                    data = json.dumps(data)
                    info = [{"ObjectTracking": data,"DebugMode" : DebugMode,"MinScore": dic["MinScore"], "MinDetectionSize": dic["MinDetectionSize"], "MemoryUsage": dic["MemoryUsage"], "model_path": dic["model_path"]}]
                    createModel(dic["name"], dic["IP"], dic["Port"], info,"detec")
            except:
                #createLog("There are errors on the imput json")
                logger = helpers.get_logger('DTS_Procesor')
                logger.warning("There are errors on the imput json")
                 
    else:
        #createLog("Error, this IP and Port are been used by other service")
        logger = helpers.get_logger('DTS_Procesor')
        logger.error("Error, this IP and Port are been used by other service")
         

    return json.dumps(result)

@app.route("/getActive", methods=["GET"])
def getActive():
    
    #createLog("Recieved getActive, showing activated Microservices")
    logger = helpers.get_logger('DTS_Procesor')
    logger.info("Recieved getActive, showing activated Microservices")

    #createLog("Activated Microservices" + str(activateModels))
    logger = helpers.get_logger('DTS_Procesor')
    logger.info("Activated Microservices" + str(activateModels))

    result = {'activatedModels': activateModels}
    return json.dumps(result)
    