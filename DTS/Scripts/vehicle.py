import time


class Vehicle(object):

    def __init__(self, id):
        self.id = id
        self.init_time = time.time()
        self.end_time = 0
        self.numAxles = 0
        self.numAxlesSimples = 0
        self.numAxlesDoubles = 0
        self.active = False

    def addEndTime(self, endTime):
        self.end_time = endTime

    def addAxle(self, id):
        self.numAxles += 1
        print("Eje con id ", id, " añadido al vehiculo", self.id)

    def addAxleSimple(self, id):
        self.numAxlesSimples += 1
        print("Eje simple con id ", id, " añadido al vehiculo", self.id)

    def addAxleDouble(self, id):
        self.numAxlesDoubles += 1
        print("Eje double con id ", id, " añadido al vehiculo", self.id)

    def resetAxles(self):
        self.numAxles = 0

    def setActive(self, active):
        self.active = active

    def getActive(self):
        return self.active