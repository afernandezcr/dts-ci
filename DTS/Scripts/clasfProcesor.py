#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import time
import numpy as np
from PIL import Image
import tensorflow as tf
from flask import Flask, request
import datetime
import json
import os

import logging
import helpers

def correct_path(path):
    if path.endswith('/'):
        correct_path = path
    else:
        correct_path = path + '/'
    return correct_path

def check_folder(info, path):
    actual_path = correct_path(path)
    logger = helpers.get_logger('clasfProcesor')
        #Change path/info
    #change_path = actual_path + str(info)
    change_path=os.path.join(actual_path, str(info))
    if os.path.isdir(change_path):
    	print('ha entrado en el if, el directorio ya existe')
    	logger.info("ha entrado en el if") 
    	change_path = actual_path + str(info)
    else:
    	print('ha entrado en el else, crea el directorio')
    	logger.info("ha entrado en el else")
    	os.mkdir(change_path)
    	change_path = actual_path + str(info)
    print(change_path)
    logger.info(change_path)
    return change_path

def createLog(text):
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S.%f')
    print("[",st,"] -", text)

def load_graph(model_file):
    graph = tf.Graph()

    with tf.gfile.FastGFile(model_file, 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        with graph.as_default():
            _ = tf.import_graph_def(graph_def, name='')
    return graph

def load_labels(label_file):
    label = []
    proto_as_ascii_lines = tf.gfile.GFile(label_file).readlines()
    for l in proto_as_ascii_lines:
        label.append(l.rstrip())
    return label

input_height = 299
input_width = 299
input_mean = 128
input_std = 128
input_layer = "Mul"
output_layer = "final_result"
input_name = "import/" + input_layer
output_name = "import/" + output_layer
sess = 0
labels = 0
name = ""
DebugMode = False

def read_tensor_from_image_file(image_np, input_height=299, input_width=299,
                                input_mean=0, input_std=255):

    # float_caster = tf.cast(image_reader, tf.float32)
    dims_expander = tf.expand_dims(image_np, 0)
    resized = tf.image.resize_bilinear(dims_expander, [input_height, input_width])
    normalized = tf.divide(tf.subtract(resized, [input_mean]), [input_std])
    sess = tf.Session()
    result = sess.run(normalized)

    return result

# Initialize the Flask application
app = Flask(__name__)

@app.route("/init", methods=["POST"])
def run():
    global sess
    global labels
    global DebugMode
    global name
    jsondata = request.get_json()
    data = json.loads(jsondata)
    DebugMode = data[0]['DebugMode']

    labels = load_labels(data[0]['Path_labels'])
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=float(data[0]['MemoryUsage']))
    config=tf.ConfigProto(gpu_options=gpu_options)
    graph = load_graph(data[0]['model_path'])
    name = data[0]['Name']
    sess = tf.Session(graph=graph, config=config)
    #config = tf.ConfigProto()
    #config.gpu_options.allow_growth = True
    
    result = {'response': True}
    return json.dumps(result)

@app.route("/", methods=["POST"])
def getAxleClass():
    global sess
    global labels
    global name
    info = []

    img = Image.open(request.files['file']).convert('RGB')
    #img.save("test", "JPEG")
    image_np = np.array(img)

    #createLog ("Image received " + str(len(image_np[0])) + "x" + str(len(image_np)))
    logger = helpers.get_logger('clasfProcesor')
    logger.info("Image received " + str(len(image_np[0])) + "x" + str(len(image_np))) 

    #image_toClassify = np.expand_dims(image_np, axis=0)

    start_time = time.time()

    # Feed the image_data as input to the graph and get first prediction
    softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')

    predictions = sess.run(softmax_tensor, {'DecodeJpeg:0': image_np})

    end=time.time()
    milisg=(end - start_time)
    milisg=milisg*1000

    #createLog ("Inference Time " + str(milisg) + " ms")
    logger = helpers.get_logger('clasfProcesor')
    logger.info("Inference Time " + str(milisg) + " ms") 

    # Sort to show labels of first prediction in order of confidence
    top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
    results = np.squeeze(predictions)

    #print('-------------------------------------------------------------')
    #cont = 0
    for i in top_k:
        #print(labels[i], results[i])
        #if cont == 0:
        #    vType = str(labels[i])
        #    vScore = results[i]
        #    cont += 1
        if labels[i] == "coches":
            type_ = "car" 
        elif labels[i] == "motos":
            type_ = "bike" 
        else:
            type_ = labels[i]

        dataAux = {"label" : type_, "score" : float(results[i])}
        info.append(dataAux)
    #print('-------------------------------------------------------------')

    #s = str(request.files['image'])
    #if(len(s) > 0):
    #    s = (s.split("FileStorage: '", 1))[1]
    #    s = (s.split("' ('image/jpeg')>", 1))[0]
    
    if DebugMode:
        #createLog(dataAux)
        logger = helpers.get_logger('clasfProcesor')
        logger.warning(dataAux) 
    path_image = "test.jpg"
    data = {"message":"Success","result":[{"message":"success","input": path_image,"prediction": info}]}

    json_data = json.dumps(data, sort_keys=True) + '\n'

    if DebugMode:
        #createLog(json_data)
        logger = helpers.get_logger('clasfProcesor')
        logger.warning(json_data) 

    #createLog("Results sended:" + str(data))
    logger = helpers.get_logger('clasfProcesor')
    logger.info("Results sended:" + str(data)) 

    ### DESCOMENTAR PARA SALVAR LAS IMÁGENES UNA VEZ INFERIDA SU CLASE. 
    date_time = datetime.datetime.utcnow()

    year = date_time.year
    month = date_time.month
    day = date_time.day
    hour = date_time.hour
    
    path_save_images = '../ImagesModeloClasificacion'
    utilidad, modelo = name.split('_')

    #Creamos carpeta imagenesModeloClasificacion
    if os.path.isdir(path_save_images):
        pass
    else:
        os.mkdir(path_save_images)
    
    #Creamos carpeta de la utilidad: Cortina, Categorias,..
    if os.path.isdir(str(path_save_images) + '/' + str(utilidad)):
        pass
    else:
        os.mkdir(str(path_save_images) + '/' + str(utilidad))
    
    #Creamos carpeta con nombre del modelo

    path_modelo = str(path_save_images) + '/' + str(utilidad)+'/'+str(modelo)
    if os.path.isdir(path_modelo):
        pass
    else:
        os.mkdir(path_modelo)

    logger = helpers.get_logger('clasfProcesor')
    logger.info('El path que se tiene que crear es: ' + str(year) + '-' + str(month) + '-' + str(day) + '-' + str(hour)) 

    try:
        path_year = check_folder(year,path_modelo)
        path_month = check_folder(month, path_year)
        path_day = check_folder(day, path_month)
        path_save_images = check_folder(hour, path_day)
    except OSError:
        print('Ha fallado creando algún directorio.')
        logger = helpers.get_logger('clasfProcesor')
        logger.error('Ha fallado creando algún directorio.') 

    #Sort values in JSON and pick the one with high score    
    results = data["result"][0]["prediction"]
    if len(results) > 0:
        # Then, save the results on a variable because we want to see all the result and pick up the best one.        
        score = 0
        vehicle_type = None
        for result in range(len(results)):
            score_res = results[result]["score"]
            # Finally, choose the best score and the type associated.
            if score_res > score:
                score = score_res
                vehicle_type = results[result]["label"]
            #print(score, vehicle_type)

    path_to_save = correct_path(path_save_images) + vehicle_type + "/"
    # check if path is created, else create it.
    if os.path.isdir(path_to_save):
        pass
    else:
        os.mkdir(path_to_save) 
    #save
    image_save = path_to_save + str(date_time) + '.jpg'
    img.save(image_save, "JPEG")
    


    return json_data
