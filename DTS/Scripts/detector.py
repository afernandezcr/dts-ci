'''
Script to test traffic light localization and detection
'''

import numpy as np
import tensorflow as tf
from PIL import Image
import os
import time
import helpers
from glob import glob

import logging

cwd = os.path.dirname(os.path.realpath(__file__))

# Uncomment the following two lines if need to use the visualization_tunitls
#os.chdir(cwd+'/models')
#from object_detection.utils import visualization_utils as vis_util


class CarDetector(object):
    def __init__(self):

        self.car_boxes = []
        
        os.chdir(cwd)
        
        #Tensorflow localization/detection model
        PATH_TO_CKPT = '/home/portico/AI/models-master/research/object_detection/axle_model/newModel.pb'

        # setup tensorflow graph
        self.detection_graph = tf.Graph()

        # configuration for possible GPU use
        #gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.1)
        #config = tf.ConfigProto(gpu_options=gpu_options)
        self.config = tf.ConfigProto()
        self.config.gpu_options.allow_growth = True
        # load frozen tensorflow detection model and initialize
        # the tensorflow graph
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
               serialized_graph = fid.read()
               od_graph_def.ParseFromString(serialized_graph)
               tf.import_graph_def(od_graph_def, name='')

            self.sess = tf.Session(graph=self.detection_graph, config=self.config)
            self.image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
              # Each box represents a part of the image where a particular object was detected.
            self.boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
              # Each score represent how level of confidence for each of the objects.
              # Score is shown on the result image, together with the class label.
            self.scores =self.detection_graph.get_tensor_by_name('detection_scores:0')
            self.classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
            self.num_detections =self.detection_graph.get_tensor_by_name('num_detections:0')

    # Helper function to convert image into numpy array
    def load_image_into_numpy_array(self, image):
         (im_width, im_height) = image.size
         return np.array(image.getdata()).reshape(
            (im_height, im_width, 3)).astype(np.uint8)

    # Helper function to convert normalized box coordinates to pixels
    def box_normal_to_pixel(self, box, dim):

        height, width = dim[0], dim[1]
        box_pixel = [int(box[0]*height), int(box[1]*width), int(box[2]*height), int(box[3]*width)]
        return np.array(box_pixel)

    def getAxleLocalization(self, image, Framecount = 0):
        """Determines the locations of the axle on image

        Args:
            image: np image

        Returns:
            list of bounding boxes: coordinates [y_up, x_left, y_down, x_right]

        """
        with self.detection_graph.as_default():

                image_np_expanded = np.expand_dims(image, axis=0)
                # Actual detection.
                (boxes, scores, classes, num) = self.sess.run(
                    [self.boxes, self.scores, self.classes, self.num_detections],
                    feed_dict={self.image_tensor: image_np_expanded})

                cont = 0
                score = 0
                tmp_car_boxes = []
                for i in range(len(scores[0])):
                    if (scores[0][i]) > 0.25:
                        cont += 1
                for i in range(cont):
                    print("Este es el contador", cont)
                    ymin = boxes[0][i][0] * len(image)
                    xmin = boxes[0][i][1] * len(image[0])
                    ymax = boxes[0][i][2] * len(image)
                    xmax = boxes[0][i][3] * len(image[0])
                    score = (scores[0][i] * 100)
                    widht = int(xmax) - int(xmin)
                    widht = abs(widht)
                    high = int(ymax) - int(ymin)
                    high = abs(high)
                    if widht > 100 and high > 100 and widht < 750:
                        print("object detected!!")
                        #box = [int(ymin), int(xmin) - int(xmin)/4, int(ymax), int(xmax) + int(xmax)/4]
                        box = [int(ymin), int(xmin - widht*0.2), int(ymax), int(xmax + widht*0.2)]
                        #box = [int(ymin), int(xmin), int(ymax), int(xmax)]
                        #print ("HOLA - XMAX=", int(xmax + widht/10), "XMIN=", int(xmin - widht/10))
                        #box = [int(ymin), int(xmin), int(ymax), int(xmax)]
                        tmp_car_boxes.append(np.array(box))
                        print_id = str(int(score))
                        box_color = (0, 0, 255)
                        show_label = False
                        img = helpers.draw_box_label(image, np.array(box), box_color, show_label, print_id)
                        #name = "results/" + str(Framecount) + ".jpg"
                        #cv2.imwrite(name, img)

                self.car_boxes = tmp_car_boxes

        return self.car_boxes, int(score)


if __name__ == '__main__':
        
        det = CarDetector()
        os.chdir(cwd)
        TEST_IMAGE_PATHS= glob(os.path.join('test_images/', '*.jpg'))
        
        for i, image_path in enumerate(TEST_IMAGE_PATHS[0:2]):
            print('')
            print('*************************************************')
            
            img_full = Image.open(image_path)
            img_full_np = det.load_image_into_numpy_array(img_full)
            img_full_np_copy = np.copy(img_full_np)
            start = time.time()
            b = det.getAxleLocalization(img_full_np)
            end = time.time()
            print('Localization time: ', end-start)