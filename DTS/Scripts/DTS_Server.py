from waitress import serve
import DTS_Procesor
import xmlschema
import sys
import json
import requests
import time
import datetime
#from threading import Thread
from multiprocessing import Process

import logging
import helpers

DTS_HOST= "0.0.0.0"
DTS_PORT= 8050

def createLog(text):

    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S.%f')

    print("[",st,"] -", text)

def checkXML(xmlFile, xsdFile):

    ok=True

    try:
        my_schema=xmlschema.XMLSchema(xsdFile)
    except:
        ok=False
        #createLog("Error loading xsd")    
        logger = helpers.get_logger('DTS_Server')
        logger.error("Error loading xsd")

    try:
        ok = (my_schema.is_valid(xmlFile))
    except:
        ok=False
    return ok

def serveFunction():
    global DTS_HOST
    global DTS_PORT
    serve(DTS_Procesor.app, host=DTS_HOST, port=DTS_PORT)

filename_xml = sys.argv[1]
filename_xsd = sys.argv[2]

ok = checkXML(filename_xml, filename_xsd)

if ok:
    #createLog("The xml has been ckecked, it is ok")
    logger = helpers.get_logger('DTS_Server')
    logger.info("The xml has been ckecked, it is ok")
    
    thread = Process(target = serveFunction)
    thread.start()
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S.%f')

    #createLog("DTS_SERVER runing on " + 'http://' + DTS_HOST +':' + str(DTS_PORT))
    logger = helpers.get_logger('DTS_Server')
    logger.info("DTS_SERVER runing on " + 'http://' + DTS_HOST +':' + str(DTS_PORT))

    time.sleep(1)
    conv = [{"filename_xml" : sys.argv[1]}]
    #conv = [{'response': True}]
    s = json.dumps(conv)
    urlInit = 'http://' + DTS_HOST +':' + str(DTS_PORT) + '/init'
    req = requests.post(urlInit, json=s).json()
    thread.join()
else:
    #createLog("The xml contains errors, check it please ")
    logger = helpers.get_logger('DTS_Server')
    logger.error("The xml contains errors, check it please ")