#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# !/usr/bin/python3
# Copyright 2017 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import time
import numpy as np
import tensorflow as tf

import logging
import helpers

class axleClassifier(object):
    def __init__(self):

        self.input_height = 299
        self.input_width = 299
        self.input_mean = 128
        self.input_std = 128
        self.input_layer = "Mul"
        self.output_layer = "final_result"
        self.input_name = "import/" + self.input_layer
        self.output_name = "import/" + self.output_layer
        self.labels = self.load_labels("/home/portico/AI/models-master/clasificadorDobleRueda_inceptionV33/tf_files/retrained_labels.txt")
        self.config = tf.ConfigProto()
        self.config.gpu_options.allow_growth = True
        self.graph = self.load_graph("/home/portico/AI/models-master/clasificadorDobleRueda_inceptionV33/tf_files/retrained_graph.pb")
        self.sess = tf.Session(graph=self.graph, config=self.config)

    def load_graph(self, model_file):
        graph = tf.Graph()

        with tf.gfile.FastGFile(model_file, 'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            with graph.as_default():
                _ = tf.import_graph_def(graph_def, name='')
        return graph

    def read_tensor_from_image_file(self, image_np, input_height=299, input_width=299,
                                    input_mean=0, input_std=255):

        # float_caster = tf.cast(image_reader, tf.float32)
        dims_expander = tf.expand_dims(image_np, 0)
        resized = tf.image.resize_bilinear(dims_expander, [input_height, input_width])
        normalized = tf.divide(tf.subtract(resized, [input_mean]), [input_std])
        sess = tf.Session()
        result = sess.run(normalized)

        return result

    def load_labels(self, label_file):
        label = []
        proto_as_ascii_lines = tf.gfile.GFile(label_file).readlines()
        for l in proto_as_ascii_lines:
            label.append(l.rstrip())
        return label

    def getAxleClass(self, image_toClassify):

        start = time.time()

        # Feed the image_data as input to the graph and get first prediction
        softmax_tensor = self.sess.graph.get_tensor_by_name('final_result:0')

        predictions = self.sess.run(softmax_tensor, {'DecodeJpeg:0': image_toClassify})

        # Sort to show labels of first prediction in order of confidence
        top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
        results = np.squeeze(predictions)

        print('-------------------------------------------------------------')
        cont = 0
        for i in top_k:
            print(self.labels[i], results[i])
            if cont == 0:
                vType = str(self.labels[i])
                vScore = results[i]
                cont += 1
        print('-------------------------------------------------------------')

        #vType = str("simple")
        #vScore = 99.5
        end = time.time()
        milisg = (end - start)
        milisg = milisg * 1000
        print("\n---%s Clasificador ms -----" % milisg)

        return vType, float(vScore)