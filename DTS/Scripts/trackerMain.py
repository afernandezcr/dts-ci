#!/usr/bin/python3
from flask import Flask, request, Response
import numpy as np
import cv2
import time
#from moviepy.editor import VideoFileClip
from sklearn.utils.linear_assignment_ import linear_assignment
from PIL import Image
import json
import datetime
import xmlschema
import vehicle as vh
import helpers
import detector
import tracker

from queue import Queue

import logging 

# Global variables to be used by funcitons of VideoFileClop
frame_count = 0 # frame counter
axle_id = 0
axle_count = 0
double_axle_count = 0
simple_axle_count = 0
maxaxle_id = 0
tracker_list = []
vehicleCount = 0
lastWhitePixels = 0
newVehicle = vh.Vehicle(0)
text = ""
resultFrame = 0
vehicle_id = 0
timeVehicleid = time.time()

debug = False

max_age = 6  # no.of consecutive unmatched detection before a track is deleted
min_hits = 2  # no. of consecutive matches needed to establish a track

det = 0
#clasf = 0

#limage = Image.open('logo_indra.jpg')

def checkXML(xmlFile, xsdFile):

    ok=True

    try:
        my_schema = xmlschema.XMLSchema(xsdFile)
    except:
        ok=False
        print("Problema al cargar el xsd")
    try:
        print(my_schema.is_valid(xmlFile))
    except:
        ok=False
        print("Se ha comprobado el archivo xml con el xsd y es incorrecto")
    return ok


#if __name__ == "__main__":

# Initialize the Flask application
app = Flask(__name__)

@app.route("/init", methods=["POST"])
def run():

    global det
    #global clasf

    det = detector.CarDetector()
    #clasf = classifier.axleClassifier()

    jsondata = request.get_json()
    data = json.loads(jsondata)

    DebugMode = data[0]['DebugMode']

    result = {'response': True}
    return json.dumps(result)


#cap = capture_frame.Capture_frame()
#capture_queue = Queue()

#worker = Thread(target=cap.captureFrame, args=(capture_queue,))
#worker.setDaemon(True)
#worker.start()

def add_logo(mimage, limage):

    # resize logo
    wsize = int(min(mimage.size[0], mimage.size[1]) * 0.35)
    wpercent = (wsize / float(limage.size[0]))
    hsize = int((float(limage.size[1]) * float(wpercent)))

    simage = limage.resize((wsize, hsize))
    mbox = mimage.getbbox()
    sbox = simage.getbbox()

    # right bottom corner
    box = (int(mimage.size[0]/2 - wsize/2 - 80), mbox[3] - sbox[3])
    mimage.paste(simage, box)

    return mimage

def assign_detections_to_trackers(trackers, detections, iou_thrd = 0.01):
    '''
    From current list of trackers and new detections, output matched detections,
    unmatchted trackers, unmatched detections.
    '''

    global reverse

    IOU_mat = np.zeros((len(trackers), len(detections)), dtype=np.float32)
    for t, trk in enumerate(trackers):
        # trk = convert_to_cv2bbox(trk)
        for d, det in enumerate(detections):
            # det = convert_to_cv2bbox(det)
            #print("Objeto : ", det)
            IOU_mat[t, d] = helpers.box_iou2(trk, det)
    #print("Tamaño trk: ", len(trackers))
    #print("Tamaño det: ", len(detections))
    #print(IOU_mat)
    # Produces matches       
    # Solve the maximizing the sum of IOU assignment problem using the
    # Hungarian algorithm (also known as Munkres algorithm)
    
    matched_idx = linear_assignment(-IOU_mat)        

    unmatched_trackers, unmatched_detections = [], []
    for t, trk in enumerate(trackers):
        if t not in matched_idx[:, 0]:
            unmatched_trackers.append(t)

    for d, det in enumerate(detections):
        if d not in matched_idx[:, 1]:
            unmatched_detections.append(d)

    matches = []
   
    # For creating trackers we consider any detection with an 
    # overlap less than iou_thrd to signifiy the existence of 
    # an untracked object
    
    for m in matched_idx:
        if IOU_mat[m[0],m[1]]<iou_thrd:
            unmatched_trackers.append(m[0])
            unmatched_detections.append(m[1])
        else:
            matches.append(m.reshape(1, 2))

    if len(matches) == 0:
        matches = np.empty((0, 2), dtype=int)
    else:
        matches = np.concatenate(matches, axis=0)
    
    return matches, np.array(unmatched_detections), np.array(unmatched_trackers)


def backgroundSub(img, first_gray):

    global vehicleCount
    global newVehicle
    global lastWhitePixels
    global text

    # pil_im = Image.fromarray(frame)
    # frame = np.array(add_logo(pil_im, limage))
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # gray_frame = gray_frame[300:len(gray_frame), 0:len(gray_frame[0])]
    gray_frame = gray_frame[265:len(gray_frame), 190:760]
    gray_frame = cv2.GaussianBlur(gray_frame, (5, 5), 0)

    difference = cv2.absdiff(first_gray, gray_frame)
    _, difference = cv2.threshold(difference, 50, 255, cv2.THRESH_BINARY)
    kernel = np.ones((2, 2), np.uint8)
    difference = cv2.erode(difference, kernel, iterations=1)

    whitePixels = np.count_nonzero(difference == 255)
    # print(whitePixels)

    font = cv2.FONT_HERSHEY_DUPLEX
    font_size = 2
    box_color = (255, 0, 0)
    #cv2.putText(img, str(whitePixels), (50, len(img) - 300), font, font_size, box_color, 3, cv2.LINE_AA)

    if whitePixels < 20:
        first_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # first_gray = first_gray[300:len(first_gray), 0:len(first_gray[0])]
        first_gray = first_gray[265:len(first_gray), 190:760]
        first_gray = cv2.GaussianBlur(first_gray, (5, 5), 0)

    if not newVehicle.getActive() and whitePixels >= 6000:
        print("Vehicle Start")
        newVehicle = vh.Vehicle(vehicleCount)
        newVehicle.setActive(True)

    elif newVehicle.getActive() and lastWhitePixels < 3000 and whitePixels < 3000:
    #elif newVehicle.getActive() and (frame_count == 2895 or frame_count == 3027 or frame_count == 3109 or frame_count == 3165 or frame_count == 3195):
        print("Vehicle End")
        print("Vehicle ID:", vehicleCount)
        newVehicle.addEndTime(time.time())
        vehicleCount += 1
        print(newVehicle.id)
        text = "Vehicle with " + str(newVehicle.numAxles) + " axles. " + str(newVehicle.numAxlesSimples) + " simples " + str(newVehicle.numAxlesDoubles) + " doubles"
        print (text)
        newVehicle.setActive(False)
        newVehicle.resetAxles()

    font = cv2.FONT_HERSHEY_DUPLEX
    font_size = 2
    box_color = (255, 255, 255)

    if not newVehicle.getActive():
        cv2.putText(img, text, (50, len(img) - 240), font, font_size, box_color, 3, cv2.LINE_AA)
    else:
        cv2.putText(img, "Updating...", (50, len(img) - 240), font, font_size, box_color, 3, cv2.LINE_AA)

    lastWhitePixels = whitePixels

    cv2.namedWindow('difference', 0)
    cv2.resizeWindow('difference', 1280, 1024)
    cv2.imshow("difference", difference)

#def pipeline(img, first_gray):
def pipeline(img):
    '''
    Pipeline function for detection and tracking
    '''
    global frame_count
    global tracker_list
    global max_age
    global min_hits
    global track_id_list
    global debug
    global axle_id
    global axle_count
    global double_axle_count
    global simple_axle_count
    global maxaxle_id
    global capture_queue
    global vehicle_id
    global timeVehicleid
    global det
    #global clasf

    info = []

    #global keepLine
    #img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    #cv2.imwrite("./ejes2/bgr%d.jpg" % (frame_count), image)

    #if frame_count == 2760:
    #    axle_count = 0
    #    axle_id = 0
    #    double_axle_count = 0
    #    simple_axle_count = 0
    #    maxaxle_id = 0

    #global limage

    #pil_im = Image.fromarray(img)
    #img = np.array(add_logo(pil_im, limage))

    frame_count += 1

    #backgroundSub(img, first_gray)

    #cv2.namedWindow('difference', 0)
    #cv2.resizeWindow('difference', 1280, 1024)
    #cv2.imshow("difference", difference)

    # get the positions of frames on img
    z_box, score = det.getAxleLocalization(img, frame_count)

    print('------------------------------------------------- Frame:', frame_count, '---------------------------------------------')
    pison_color = (0, 0, 255)
    pison_active = 0

    # frame list of track objects
    x_box =[]
    if len(tracker_list) > 0:
        for trk in tracker_list:
            x_box.append(trk.box)

    matched, unmatched_dets, unmatched_trks = assign_detections_to_trackers(x_box, z_box, iou_thrd=0.01)
    if not debug:
        print('Detection: ', z_box)
        print('x_box: ', x_box)
        print('matched:', matched)
        print('unmatched_det:', unmatched_dets)
        print('unmatched_trks:', unmatched_trks)

    # Deal with matched detections     
    if matched.size > 0:
        for trk_idx, det_idx in matched:
            z = z_box[det_idx]
            z = np.expand_dims(z, axis=0).T
            tmp_trk= tracker_list[trk_idx]
            tmp_trk.kalman_filter(z)
            xx = tmp_trk.x_state.T[0].tolist()
            xx = [xx[0], xx[2], xx[4], xx[6]]
            x_box[trk_idx] = xx
            tmp_trk.box = xx
            tmp_trk.hits += 1
            tmp_trk.score = score

    # Deal with unmatched detections      
    if len(unmatched_dets) > 0:
        for idx in unmatched_dets:
            z = z_box[idx]
            z = np.expand_dims(z, axis=0).T
            tmp_trk = tracker.Tracker() # Create a new tracker
            x = np.array([[z[0], 0, z[1], 0, z[2], 0, z[3], 0]]).T
            tmp_trk.x_state = x
            tmp_trk.predict_only()
            xx = tmp_trk.x_state
            xx = xx.T[0].tolist()
            xx = [xx[0], xx[2], xx[4], xx[6]]
            tmp_trk.box = xx
            tmp_trk.id = axle_id
            tmp_trk.simple = 0
            tmp_trk.double = 0
            tmp_trk.crossLine = False
            tmp_trk.score = score

            print("Se crea un nuevo eje con id: ", axle_id)

            # pison equation to extract x
            x_pison = (tmp_trk.box[0] - 833.51) / (-1.67)

            if (tmp_trk.box[3] + tmp_trk.box[1])/2 < x_pison:
                tmp_trk.correct_way = True
            else:
                tmp_trk.correct_way = False

            axle_id += 1
            print("El id es: ", axle_id)
            tracker_list.append(tmp_trk)
            x_box.append(xx)

    # Deal with unmatched tracks       
    if len(unmatched_trks) > 0:
        for trk_idx in unmatched_trks:
            tmp_trk = tracker_list[trk_idx]
            tmp_trk.no_losses += 1
            tmp_trk.predict_only()
            xx = tmp_trk.x_state
            xx = xx.T[0].tolist()
            xx =[xx[0], xx[2], xx[4], xx[6]]
            tmp_trk.box = xx
            x_box[trk_idx] = xx
            tmp_trk.score = score

    # The list of tracks to be annotated  
    good_tracker_list = []
    for trk in tracker_list:
        if (trk.hits >= min_hits) and (trk.no_losses <= max_age):
            good_tracker_list.append(trk)

            x_cv2 = trk.box.copy()
            widht = int(trk.box[3]) - int(trk.box[1])
            widht = abs(widht)
            x_cv2[1] = int(trk.box[1] + (widht*0.15))
            x_cv2[3] = int(trk.box[3] - (widht*0.15))

            box_color = (255, 0, 0)
            show_label = False
            double = False

            if x_cv2[1] > 0:
                imgAux = img
                roi_axle_image = imgAux[x_cv2[0]:x_cv2[2], x_cv2[1]:x_cv2[3]]
                # cv2.imwrite("./ejes2/frame%d%d.jpg" %(trk.id, frame_count), roi_axle_image)

                if len(roi_axle_image) > 0 and len(roi_axle_image[0]) > 5:
                    #type, score = clasf.getAxleClass(roi_axle_image)
                    type, score = "double", 99.0

                    #if type == "double":
                        #cv2.imwrite("./ejes2/frame%d%d.jpg" % (trk.id, frame_count), roi_axle_image)

                    if type == "double" and score > 0.90:
                        box_color = (0, 255, 0)
                        double = True
                        trk.double += 1
                    else:
                        trk.simple += 1

            # pison equation to extract x
            #x_pison = (tmp_trk.box[0] + 792.5) / (1.65)
            x_pison = (tmp_trk.box[0] - 959) / (-0.953)

            print("El eje con id:", trk.id, "sentido", trk.correct_way, "y su x es:", (trk.box[3] + trk.box[1]) / 2)

            #if trk.id + 1 > maxaxle_id:
            # New axle in correct way
            if trk.crossLine == False and trk.correct_way == True and (trk.box[3] + trk.box[1])/2 < x_pison:
                trk.crossLine = True
                pison_color = (0, 255, 0)
                pison_active = 1
                #keepLine = 4
                # Capture Frame by Hikvision for each axle
                #print('Queuing:', time.time())
                #capture_queue.put(time.time())
                print("El x es ", (trk.box[3] + trk.box[1])/2)
                print("El x del pison ", x_pison)
                print("SIGNAL 1 SEND!!!!!!!!!!!!!!!!!!!!!!")
                newVehicle.addAxle(trk.id)
                axle_count += 1
                maxaxle_id = trk.id + 1

                if trk.double >= 2: #trk.simple:
                    double_axle_count += 1
                    newVehicle.addAxleDouble(trk.id)
                else:
                    simple_axle_count += 1
                    newVehicle.addAxleSimple(trk.id)
            # New axle reverse
            if trk.crossLine == False and trk.correct_way == False and (trk.box[3] + trk.box[1]) / 2 > x_pison:
                trk.crossLine = True
                pison_color = (0, 255, 0)
                pison_active = 1
                #keepLine = 4
                # Capture Frame by Hikvision for each axle
                #print('Queuing:', time.time())
                #capture_queue.put(time.time())
                print("SIGNAL 2 SEND!!!!!!!!!!!!!!!!!!!!!!")
                newVehicle.addAxle(trk.id)
                axle_count -= 1
                maxaxle_id = trk.id + 1

                if trk.double >= 2: #trk.simple:
                    double_axle_count += 1
                    newVehicle.addAxleDouble(trk.id)
                else:
                    simple_axle_count += 1
                    newVehicle.addAxleSimple(trk.id)

            # New axle reverse way and first time on the left of the line
            if trk.crossLine == False and trk.correct_way == False and (
                trk.box[3] + trk.box[1]) / 2 > x_pison:
                trk.crossLine = True
                #its not necessary because de vehicle has not crossed the line yet
                pison_color = (0, 255, 0)
                pison_active = 1
                print("SIGNAL 3 SEND!!!!!!!!!!!!!!!!!!!!!!")
                newVehicle.addAxle(trk.id)
                axle_count += 1
                maxaxle_id = trk.id + 1

                if trk.double >= 2: #trk.simple:
                    double_axle_count += 1
                    newVehicle.addAxleDouble(trk.id)
                else:
                    simple_axle_count += 1
                    newVehicle.addAxleSimple(trk.id)

            #else:
            # Existing axle reverse but first time on the left of the line
            if trk.crossLine == False and trk.correct_way == False and (trk.box[3] + trk.box[1]) / 2 < x_pison:
                trk.crossLine = True
                pison_color = (0, 255, 0)
                pison_active = 1
                #keepLine = 4
                # Capture Frame by Hikvision for each axle
                #print('Queuing:', time.time())
                #capture_queue.put(time.time())
                print("SIGNAL 4 SEND!!!!!!!!!!!!!!!!!!!!!!")
                newVehicle.addAxle(trk.id)
                axle_count += 1

                if trk.double >= 2: #trk.simple:
                    double_axle_count += 1
                    newVehicle.addAxleDouble(trk.id)
                else:
                    simple_axle_count += 1
                    newVehicle.addAxleSimple(trk.id)

            # Existing axle reverse
            if trk.crossLine == True and trk.correct_way == True and (trk.box[3] + trk.box[1]) / 2 > x_pison:
                trk.crossLine = False
                pison_color = (0, 255, 0)
                pison_active = 1
                #keepLine = 4
                # Capture Frame by Hikvision for each axle
                #print('Queuing:', time.time())
                #capture_queue.put(time.time())
                print("SIGNAL 5 SEND!!!!!!!!!!!!!!!!!!!!!!")
                newVehicle.addAxle(trk.id)
                axle_count -= 1

                if trk.double >= 2:  # trk.simple:
                    double_axle_count += 1
                    newVehicle.addAxleDouble(trk.id)
                else:
                    simple_axle_count += 1
                    newVehicle.addAxleSimple(trk.id)

            # Existing axle reverse but first time on the left of the line
            if trk.crossLine == False and trk.correct_way == False and (trk.box[3] + trk.box[1]) / 2 < x_pison:
                #trk.crossLine = False
                pison_color = (0, 255, 0)
                pison_active = 1
                #keepLine = 4
                # Capture Frame by Hikvision for each axle
                #print('Queuing:', time.time())
                #capture_queue.put(time.time())
                print("SIGNAL 6 SEND!!!!!!!!!!!!!!!!!!!!!!")
                newVehicle.addAxle(trk.id)
                axle_count -= 1

                if trk.double >= 2:  # trk.simple:
                    double_axle_count += 1
                    newVehicle.addAxleDouble(trk.id)
                else:
                    simple_axle_count += 1
                    newVehicle.addAxleSimple(trk.id)

            print_id = trk.id + 1

            print("Frame: ", frame_count, " id: ", print_id)
            print ("AxleCount: ", axle_count)

            if debug:
                print('updated box: ', x_cv2)
                print()

            print(x_cv2)

            newTime = time.time()

            print (newTime - timeVehicleid)

            if(newTime - timeVehicleid) > 2:
                timeVehicleid = newTime
                vehicle_id+=1


            img = helpers.draw_box_label(img, x_cv2, box_color, show_label, print_id) # Draw the bounding boxes on the
            dataAux = {"label": "axle", "xmin": int(x_cv2[1]), "ymin": int(x_cv2[0]), "xmax": int(x_cv2[3]), "ymax": int(x_cv2[2]),
                       "score": float(trk.score), "correct_way": int(trk.correct_way), "axle_id": print_id, "pison_activate": pison_active, "vehicle_id": vehicle_id}
            info.append(dataAux)

        pison_active = 0

    # Book keeping
    deleted_tracks = filter(lambda x: x.no_losses > max_age, tracker_list)

    # for trk in deleted_tracks:
    #        track_id_list.append(trk.id)

    tracker_list = [x for x in tracker_list if x.no_losses<=max_age]

    if debug:
        print('Ending tracker_list: ', len(tracker_list))
        print('Ending good tracker_list: ', len(good_tracker_list))

    #pison line
    #cv2.line(img, (544, 270), (0, 592), pison_color, 5)
    #if keepLine > 0:
    #    pison_color = (0, 255, 0)
    #    keepLine -= 1

    #cv2.line(img, (544, 270), (0, 592), pison_color, 3)
    cv2.line(img, (193, 775), (729, 264), pison_color, 3)

    font = cv2.FONT_HERSHEY_DUPLEX
    font_size = 2
    box_color = (255, 255, 255)
    totalAxleText = "Total: " + str(axle_count)
    simpleAxleText = "Simple: " + str(simple_axle_count)
    doubleAxleText = "Double: " + str(double_axle_count)
    cv2.putText(img, totalAxleText, (50, len(img) - 170), font, font_size, box_color, 3, cv2.LINE_AA)
    cv2.putText(img,  simpleAxleText, (50, len(img) - 100), font, font_size, box_color, 3, cv2.LINE_AA)
    cv2.putText(img,  doubleAxleText, (50, len(img) - 30), font, font_size, box_color, 3, cv2.LINE_AA)
    #cv2.imwrite("./results/frame%d.jpg" % frame_count, img)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

    s = str(request.files['file'])
    if (len(s) > 0):
        s = (s.split("FileStorage: '", 1))[1]
        s = (s.split("' ('image/jpeg')>", 1))[0]

    path_image = s

    data = {"message": "Success", "result": [{"message": "success", "input": path_image, "prediction": info}]}
    json_data = json.dumps(data, sort_keys=True) + '\n'

    return json_data, img

def gen():
    global resultFrame
    while True:
        frame = resultFrame
        ret, jpeg = cv2.imencode('.jpg', frame)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + jpeg.tobytes() + b'\r\n\r\n')

@app.route('/video_feed')
def video_feed():
    return Response(gen(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route("/", methods=["POST"])
def home():
    global resultFrame

    start = time.time()
    img = Image.open(request.files['file']).convert('RGB')
    # img.save("test", "JPEG")
    image_np = np.array(img)

    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S.%f')
    print ("[", st, "] -", "Image received", len(image_np[0]), "x", len(image_np))

    json_data, image_box = pipeline(image_np)

    resultFrame = image_box

    #name = "serverTest/" + str(frame_count) + ".jpg"
    #cv2.imwrite(name, image_box)

    #cv2.namedWindow('Frame', 0)
    #cv2.resizeWindow('Frame', 1280, 1024)
    #cv2.imshow("Frame", image_box)

    #key = cv2.waitKey(1)
    #if key == 27:
    #    cv2.destroyAllWindows()

    end = time.time()
    print(round(end - start, 2), 'Seconds to finish')

    return json_data
